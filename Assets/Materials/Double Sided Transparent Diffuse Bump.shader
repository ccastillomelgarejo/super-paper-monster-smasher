// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'



Shader "Custom/Double Sided Transparent Diffuse Bump" {
Properties {
_Diffusecolor ("Diffuse color", Color) = (1,1,1,1)
_DiffuseMapTransA ("Diffuse Map (Trans A)", 2D) = "white" {}
_NormalMap ("Normal Map", 2D) = "bump" {}
_NormalIntensity ("Normal Intensity", Range(0, 1)) = 1
_SpecularMap ("Specular Map", 2D) = "white" {}
_Speccolor ("Spec color", Color) = (1,1,1,1)
_SpecIntensity ("Spec Intensity", Range(0, 2)) = 1
_Gloss ("Gloss", Range(0, 1)) = 0.5
_Transparency ("Transparency", Range(0, 1)) = 0.5
[HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
}
SubShader {
Tags {
"Queue"="Transparent"
"RenderType"="Transparent"
}
Pass {
    Name "FORWARD"
    Tags {
        "LightMode"="ForwardBase"
    }
    Blend SrcAlpha OneMinusSrcAlpha
    Cull Off


    CGPROGRAM
    #pragma vertex vert
    #pragma fragment frag
    #define UNITY_PASS_FORWARDBASE
    #include "UnityCG.cginc"
    #include "AutoLight.cginc"
    #pragma multi_compile_fwdbase_fullshadows
    #pragma multi_compile_fog
    #pragma exclude_renderers xbox360 ps3 
    #pragma target 3.0
    uniform float4 _LightColor0;
    uniform sampler2D _DiffuseMapTransA; uniform float4 _DiffuseMapTransA_ST;
    uniform float4 _Diffusecolor;
    uniform sampler2D _NormalMap; uniform float4 _NormalMap_ST;
    uniform float _NormalIntensity;
    uniform sampler2D _SpecularMap; uniform float4 _SpecularMap_ST;
    uniform float4 _Speccolor;
    uniform float _SpecIntensity;
    uniform float _Gloss;
    uniform float _Transparency;
    struct VertexInput {
        float4 vertex : POSITION;
        float3 normal : NORMAL;
        float4 tangent : TANGENT;
        float2 texcoord0 : TEXCOORD0;
    };
    struct VertexOutput {
        float4 pos : SV_POSITION;
        float2 uv0 : TEXCOORD0;
        float4 posWorld : TEXCOORD1;
        float3 normalDir : TEXCOORD2;
        float3 tangentDir : TEXCOORD3;
        float3 bitangentDir : TEXCOORD4;
        LIGHTING_COORDS(5,6)
        UNITY_FOG_COORDS(7)
    };
    VertexOutput vert (VertexInput v) {
        VertexOutput o = (VertexOutput)0;
        o.uv0 = v.texcoord0;
        o.normalDir = UnityObjectToWorldNormal(v.normal);
        o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
        o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
        o.posWorld = mul(unity_ObjectToWorld, v.vertex);
        float3 lightColor = _LightColor0.rgb;
        o.pos = UnityObjectToClipPos(v.vertex);
        UNITY_TRANSFER_FOG(o,o.pos);
        TRANSFER_VERTEX_TO_FRAGMENT(o)
        return o;
    }
    float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
        float isFrontFace = ( facing >= 0 ? 1 : 0 );
        float faceSign = ( facing >= 0 ? 1 : -1 );
        i.normalDir = normalize(i.normalDir);
        i.normalDir *= faceSign;
        float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
        /////// Vectors:
        float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
        float3 _NormalMap_var = UnpackNormal(tex2D(_NormalMap,TRANSFORM_TEX(i.uv0, _NormalMap)));
        float3 normalLocal = lerp(float3(0,0,1),_NormalMap_var.rgb,_NormalIntensity);
        float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
        float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
        float3 lightColor = _LightColor0.rgb;
        float3 halfDirection = normalize(viewDirection+lightDirection);
        ////// Lighting:
        float attenuation = LIGHT_ATTENUATION(i);
        float3 attenColor = attenuation * _LightColor0.xyz;
        ///////// Gloss:
        float gloss = lerp(0.3,1,_Gloss);
        float specPow = exp2( gloss * 10.0+1.0);
        ////// Specular:
        float NdotL = max(0, dot( normalDirection, lightDirection ));
        float4 _SpecularMap_var = tex2D(_SpecularMap,TRANSFORM_TEX(i.uv0, _SpecularMap));
        float3 specularColor = ((_SpecularMap_var.rgb*_SpecIntensity)*_Speccolor.rgb);
        float3 directSpecular = (floor(attenuation) * _LightColor0.xyz) * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularColor;
        float3 specular = directSpecular;
        /////// Diffuse:
        NdotL = max(0.0,dot( normalDirection, lightDirection ));
        float3 directDiffuse = max( 0.0, NdotL) * attenColor;
        float3 indirectDiffuse = float3(0,0,0);
        indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb; // Ambient Light
        float4 _DiffuseMapTransA_var = tex2D(_DiffuseMapTransA,TRANSFORM_TEX(i.uv0, _DiffuseMapTransA));
        float3 diffuseColor = (_DiffuseMapTransA_var.rgb*_Diffusecolor.rgb);
        float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
        /// Final Color:
        float3 finalColor = diffuse + specular;
        fixed4 finalRGBA = fixed4(finalColor,lerp(0.0,_DiffuseMapTransA_var.a,_Transparency));
        UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
        return finalRGBA;
    }
    ENDCG
}

Pass {
    Name "FORWARD_DELTA"
    Tags {
        "LightMode"="ForwardAdd"
    }
    Blend One One
    Cull Off


    CGPROGRAM
    #pragma vertex vert
    #pragma fragment frag
    #define UNITY_PASS_FORWARDADD
    #include "UnityCG.cginc"
    #include "AutoLight.cginc"
    #pragma multi_compile_fwdadd_fullshadows
    #pragma multi_compile_fog
    #pragma exclude_renderers xbox360 ps3 
    #pragma target 3.0
    uniform float4 _LightColor0;
    uniform sampler2D _DiffuseMapTransA; uniform float4 _DiffuseMapTransA_ST;
    uniform float4 _Diffusecolor;
    uniform sampler2D _NormalMap; uniform float4 _NormalMap_ST;
    uniform float _NormalIntensity;
    uniform sampler2D _SpecularMap; uniform float4 _SpecularMap_ST;
    uniform float4 _Speccolor;
    uniform float _SpecIntensity;
    uniform float _Gloss;
    uniform float _Transparency;
    struct VertexInput {
        float4 vertex : POSITION;
        float3 normal : NORMAL;
        float4 tangent : TANGENT;
        float2 texcoord0 : TEXCOORD0;
    };
    struct VertexOutput {
        float4 pos : SV_POSITION;
        float2 uv0 : TEXCOORD0;
        float4 posWorld : TEXCOORD1;
        float3 normalDir : TEXCOORD2;
        float3 tangentDir : TEXCOORD3;
        float3 bitangentDir : TEXCOORD4;
        LIGHTING_COORDS(5,6)
        UNITY_FOG_COORDS(7)
    };
    VertexOutput vert (VertexInput v) {
        VertexOutput o = (VertexOutput)0;
        o.uv0 = v.texcoord0;
        o.normalDir = UnityObjectToWorldNormal(v.normal);
        o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
        o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
        o.posWorld = mul(unity_ObjectToWorld, v.vertex);
        float3 lightColor = _LightColor0.rgb;
        o.pos = UnityObjectToClipPos(v.vertex);
        UNITY_TRANSFER_FOG(o,o.pos);
        TRANSFER_VERTEX_TO_FRAGMENT(o)
        return o;
    }
    float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
        float isFrontFace = ( facing >= 0 ? 1 : 0 );
        float faceSign = ( facing >= 0 ? 1 : -1 );
        i.normalDir = normalize(i.normalDir);
        i.normalDir *= faceSign;
        float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
        /////// Vectors:
        float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
        float3 _NormalMap_var = UnpackNormal(tex2D(_NormalMap,TRANSFORM_TEX(i.uv0, _NormalMap)));
        float3 normalLocal = lerp(float3(0,0,1),_NormalMap_var.rgb,_NormalIntensity);
        float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
        float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
        float3 lightColor = _LightColor0.rgb;
        float3 halfDirection = normalize(viewDirection+lightDirection);
        ////// Lighting:
        float attenuation = LIGHT_ATTENUATION(i);
        float3 attenColor = attenuation * _LightColor0.xyz;
        ///////// Gloss:
        float gloss = lerp(0.3,1,_Gloss);
        float specPow = exp2( gloss * 10.0+1.0);
        ////// Specular:
        float NdotL = max(0, dot( normalDirection, lightDirection ));
        float4 _SpecularMap_var = tex2D(_SpecularMap,TRANSFORM_TEX(i.uv0, _SpecularMap));
        float3 specularColor = ((_SpecularMap_var.rgb*_SpecIntensity)*_Speccolor.rgb);
        float3 directSpecular = attenColor * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularColor;
        float3 specular = directSpecular;
        /////// Diffuse:
        NdotL = max(0.0,dot( normalDirection, lightDirection ));
        float3 directDiffuse = max( 0.0, NdotL) * attenColor;
        float4 _DiffuseMapTransA_var = tex2D(_DiffuseMapTransA,TRANSFORM_TEX(i.uv0, _DiffuseMapTransA));
        float3 diffuseColor = (_DiffuseMapTransA_var.rgb*_Diffusecolor.rgb);
        float3 diffuse = directDiffuse * diffuseColor;
        /// Final Color:
        float3 finalColor = diffuse + specular;
        fixed4 finalRGBA = fixed4(finalColor * lerp(0.0,_DiffuseMapTransA_var.a,_Transparency),0);
        UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
        return finalRGBA;
    }
    ENDCG
}
}
FallBack "Diffuse"
CustomEditor "ShaderForgeMaterialInspector"
}
