using UnityEngine;
using System.Collections;

public class Blink : MonoBehaviour {

    public float delay = .5f;


    void Start () {
        StartCoroutine(OnBlink());
    }

    IEnumerator OnBlink(){
        yield return new WaitForSeconds(delay);
        float alpha = transform.GetComponent<Renderer>().material.color.a;
        var newAlpha = 0f;
        if (alpha != 1) {
            newAlpha = 1f;
        }
        transform.GetComponent<Renderer>().material.color = new Color(1, 1, 1, newAlpha);
        StartCoroutine(OnBlink());
    }
}
