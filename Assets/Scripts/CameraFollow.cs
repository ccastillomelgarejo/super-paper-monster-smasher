using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

    private Vector2 velocity;

    public GameObject target;
    private Transform targetPos;
    public float smoothTime;

    private float rightBound;
    private float leftBound;

    private SpriteRenderer groundBounds;

    // Use this for initialization
    void Start () {
        groundBounds = GameObject.Find("ground").GetComponentInChildren<SpriteRenderer>();
        float vertExten =  Camera.main.GetComponent<Camera>().orthographicSize;
        float horzExten =  vertExten * Screen.width / Screen.height;

        leftBound = (float)(horzExten - groundBounds.sprite.bounds.size.x / 2.0f);
        rightBound = (float)(groundBounds.sprite.bounds.size.x / 2.0f - horzExten);

        targetPos = target.transform;

    }

    // Smooth Camera Following
    void Update () {
        if (targetPos) {
            float posX= Mathf.SmoothDamp(transform.position.x, targetPos.position.x, ref velocity.x, smoothTime);
            posX= Mathf.Clamp(posX, leftBound, rightBound);

            float posY= Mathf.SmoothDamp(transform.position.y, targetPos.position.y, ref velocity.y, smoothTime);

            transform.position = new Vector3 (posX, posY, transform.position.z);
        }
    }
}
