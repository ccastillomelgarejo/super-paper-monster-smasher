using UnityEngine;
using System.Collections;

public class ShowDamage : MonoBehaviour {

    public int damage;
    public float delay;
    public float speed = .3f;
    public GameObject target;
    public float initialOpacity = 1.0f;
    private float currentOpacity = 1.0f;
    // Use this for initialization
    void Start () {
        currentOpacity = initialOpacity;
        StartCoroutine(Destroy());
    }

    // Update is called once per frame
    void Update () {
            transform.position =  new Vector3 (transform.position.x,
                                               transform.position.y-0.005f , transform.position.z);
    }

    void OnGUI(){
        GUI.color = new Color(1.0f, 0, 0, currentOpacity);
        currentOpacity -= 0.01f;

        GUI.skin.label.fontSize=40;
        GUI.skin.label.alignment = TextAnchor.MiddleCenter;
        if(damage!=null){
            var positionComputed =Camera.main.WorldToScreenPoint(transform.position);
            GUI.Label (new Rect (positionComputed.x,positionComputed.y, 40, 40),"-"+damage.ToString());
        }
    }

    IEnumerator Destroy(){
        yield return new WaitForSeconds(delay);
        Destroy(gameObject);
    }
}
