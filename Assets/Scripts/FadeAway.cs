using UnityEngine;
using System.Collections;

public class FadeAway : MonoBehaviour {

    public float delay = 2.0f;

    void Start () {
        StartCoroutine(FadeTo(0.0f, 1.0f));
    }

    // Update is called once per frame
    void Update () {

    }

    IEnumerator FadeTo(float aValue, float aTime){
        yield return new WaitForSeconds(delay);

        float alpha = transform.GetComponent<Renderer>().material.GetFloat("_Transparency");

        for (float t = 0.0f; t <= 1.0f; t += Time.deltaTime / aTime){

            Color newColor = new Color(1, 1, 1, Mathf.Lerp(alpha,aValue,t));

            //transform.GetComponent<Renderer>().material.color = newColor;
            transform.GetComponent<Renderer>().material.SetFloat("_Transparency", Mathf.Lerp(alpha,aValue,t));

            if(newColor.a <= 0.05 && gameObject.tag!="Player")
                Destroy(gameObject);

            yield return null;
        }
    }
}
