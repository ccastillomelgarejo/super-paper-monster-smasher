using UnityEngine;
using System.Collections;

public class FlickerLight : MonoBehaviour {

    public float minFlickerIntensity   = 0.5f;
    public float maxFlickerIntensity   = 2.5f;
    public float flickerSpeed  = 0.035f;
    public float lightIntensityTarget;
    public float smoothVelocity = 0.0F;

    void Start(){
         StartCoroutine ( FlickLightIntensity());
        lightIntensityTarget = minFlickerIntensity;
    }

    void Update () {
        var currentIntensity = GetComponent<Light>().intensity;
        var newIntensity = Mathf.SmoothDamp(currentIntensity, lightIntensityTarget , ref smoothVelocity, flickerSpeed);
        GetComponent<Light>().intensity = newIntensity;
    }

    IEnumerator FlickLightIntensity(){
        yield return new WaitForSeconds (flickerSpeed);
        lightIntensityTarget = Random.Range (minFlickerIntensity, maxFlickerIntensity);
        StartCoroutine (FlickLightIntensity());
    }



}
