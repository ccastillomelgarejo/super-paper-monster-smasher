using UnityEngine;
using System.Collections;

public class Attack : MonoBehaviour {


    public int attackValue = 1;
    public float attackDelay = 1f;
    public string targetTag;
    private bool canAttack;

    public float stunDelay = 1f;
    public bool isStunned;

    public AudioClip attackClip;
    

    // Use this for initialization
    void Start () {
        if (attackValue <= 0)
            canAttack = false;
        else
            StartCoroutine(OnAttack());

        isStunned=false;

    }

    // Update is called once per frame
    void Update () {

    }

    void OnCollisionStay2D(Collision2D c){
        if (c.gameObject.tag == targetTag){
            if (canAttack){ 
                TestAttack(c.gameObject);

            }
        }
    }

    void TestAttack(GameObject target){
        if (transform.localScale.x > 0){
            if ((target.transform.position.x > transform.position.x) 
                && (target.tag!="Player" || target.transform.localScale.x>0)){
                AttackTarget(target);
            }
        }else {
            if (target.transform.position.x < transform.position.x
                && (target.tag!="Player" || target.transform.localScale.x<0)){
                AttackTarget(target);
            }
        }
        canAttack = false;
    }



    void AttackTarget(GameObject target){

        target.GetComponent<Health> ();

        target.GetComponent<Attack> ().stun();
        target.GetComponent<Rigidbody2D>().AddForce(new Vector2(transform.position.x  *-1, 0) * GetComponent<Rigidbody2D>().velocity.x, ForceMode2D.Impulse);
        
        GetComponent<Rigidbody2D>().AddForce(new Vector2(transform.position.x  *-1, 0) * GetComponent<Rigidbody2D>().velocity.x, ForceMode2D.Impulse);

        var healthComponent = target.GetComponent<Health> ();
        var audioSource = this.GetComponent<AudioSource> ();

        if (attackClip && audioSource && !audioSource.isPlaying){
            audioSource.PlayOneShot(attackClip);
        }
        if (healthComponent)
            healthComponent.TakeDamage (attackValue);
    }


    IEnumerator OnAttack(){
        yield return new WaitForSeconds (attackDelay);
        canAttack = true;
        StartCoroutine (OnAttack());
    }

    public void stun(){
        isStunned=true;
        StartCoroutine (OnStun());
    }

    IEnumerator OnStun(){
        yield return new WaitForSeconds (stunDelay);
        isStunned = false;
    }
}
