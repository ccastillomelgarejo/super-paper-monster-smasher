using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour {


    public int maxHealth = 10;
    public int health = 10;

    public int scoreToProvide = 0;
    private GameObject showScoreInstance = null;


    public GameObject deathInstance = null;
    public GameObject showDamageInstance = null;

    public Vector2 deathInstanceOffset = new Vector2(0,0);
    public Vector2 showDamageInstanceOffset = new Vector2(0,0);


    void Start () {
        health = maxHealth;
        if( scoreToProvide>0){
            showScoreInstance = GameObject.Find("ScoreUI");
        }
    }

    // Update is called once per frame
    void Update () {

    }

    public void TakeDamage(int value){
        health -= value;
        if (health <= 0){
            OnKill();
        }  
        createDamagePopUp(value);
    }

    void createDamagePopUp(int value){
        var pos = gameObject.transform.position;
        GameObject showDamage = Instantiate (showDamageInstance, new Vector3(pos.x + showDamageInstanceOffset.x,
                                                                             pos.y + showDamageInstanceOffset.y, pos.z),
                                             Quaternion.identity) as GameObject;
        showDamage.GetComponent<ShowDamage>().damage=value;
        showDamage.GetComponent<ShowDamage>().target=gameObject;
    }


    void OnKill(){
        if(showScoreInstance){
            var actualScore = showScoreInstance.GetComponent<ScoreUI>().score;
            showScoreInstance.GetComponent<ScoreUI>().score = actualScore + scoreToProvide;
        }

        if (deathInstance) {
            var pos = gameObject.transform.position;

            GameObject clone = Instantiate (deathInstance, new Vector3(pos.x +
                                                                       deathInstanceOffset.x, pos.y +
                                                                       deathInstanceOffset.y, pos.z),
                                            Quaternion.identity) as GameObject;
        }
        Destroy(gameObject);
    }
}
