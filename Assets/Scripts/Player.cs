using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

    public float speed = 200;
    public float maxSpeed = 5;
    int moving = 0;
    float mouseX = 0;
    public string groundTag;
    public AudioClip bounceClip;

    void Update () {
        moving = getMovingDirection ();
        if (moving != 0){
            var velocityX = System.Math.Abs(GetComponent<Rigidbody2D>().velocity.x);

            GetComponent<Rigidbody2D>().AddForce(new Vector2(moving, 0) * speed);
            if (this.transform.localScale.x != moving){
                this.transform.localScale = new Vector3(moving, 1, moving);
            }
            if (velocityX > maxSpeed)
                GetComponent<Rigidbody2D>().velocity = new Vector2(maxSpeed * moving, 0);
        }
    }

    void OnCollisionEnter2D(Collision2D c){
        var audioSource = this.GetComponent<AudioSource> ();
        if (c.gameObject.tag == groundTag){
            if (bounceClip && audioSource){
                audioSource.PlayOneShot(bounceClip);
            }
        }
    }


    float detectScreenTouchPosition(){
        if (Input.GetMouseButtonDown (0)) {
            return 90*((Input.mousePosition.x-Screen.width/2) / (Screen.width/2));
        }
        return 0;
    }

    int getMovingDirection(){
        mouseX = detectScreenTouchPosition();

        if (Input.GetKey("right") || mouseX > 0){
            return 1;
        }else if (Input.GetKey("left") || mouseX <0){
            return -1;
        }else {
            return 0;
        }
    }
}
