using UnityEngine;
using System.Collections;

public class MoveForward : MonoBehaviour {

    public float speed = .3f;

    // Update is called once per frame
    void Update () {
        if(!GetComponent<Attack>().isStunned){
            GetComponent<Rigidbody2D>().AddForce(new Vector2(this.transform.localScale.x, 0) * speed);
            if(System.Math.Abs(GetComponent<Rigidbody2D>().velocity.x)> speed)
                GetComponent<Rigidbody2D>().velocity = new Vector2(this.transform.localScale.x, 0) * speed;
        }else{
            if(System.Math.Abs(GetComponent<Rigidbody2D>().velocity.x)> 0){
                GetComponent<Rigidbody2D>().AddForce(new Vector2(this.transform.localScale.x, 0) * speed*2);
            }

        }
        //            GetComponent<Rigidbody2D>().AddForce(new Vector2(this.transform.localScale.x, 0) * speed*2);
        //        }
    }

}
