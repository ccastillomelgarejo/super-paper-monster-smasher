using UnityEngine;
using System.Collections;

public class ScoreUI : MonoBehaviour {

    public int score=0;
    public Vector2 offset = new Vector2();
    public Vector2 size = new Vector2();

    void OnGUI(){
        GUI.color = new Color(1.0f, 1.0f, 1.0f, 1);
        GUI.skin.label.fontSize=25;
        GUI.skin.label.alignment = TextAnchor.MiddleRight;
        GUI.Label (new Rect (Screen.width - size.x - offset.x, offset.y, size.x, size.y), "Score: "+score.ToString());
    }
}
