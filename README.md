# Super Paper Monster Smasher

A simple 2D game developed with Unity.

![](https://media.giphy.com/media/4WEUzKe6yltgEPlVeC/giphy.gif)

## LEGAL

This game has been developed by following the Jesse Freeman's tutorial from "Weekend Code Project: Unity's new 2D Workflow" book.  Some images, sounds and other resources were taken from the book assets. 
